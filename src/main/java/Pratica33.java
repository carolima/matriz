import utfpr.ct.dainf.if62c.exemplos.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {
 public static void main(String[] args) {
        Matriz orig = new Matriz(3, 2);
        double[][] m = orig.getMatriz(); 
        m[0][0] = 0.0;
        m[0][1] = 10.0;
        m[1][0] = 20.0;
        m[1][1] = 30.0;
        m[2][0] = 40.0;
        m[2][1] = 50.0;
        
        
        Matriz auxsoma = new Matriz(3, 2);
        double [][]soma = auxsoma.getMatriz();
        soma[0][0] = 10.0;
        soma[0][1] = 30.0;
        soma[1][0] = 50.0;
        soma[1][1] = 70.0;
        soma[2][0] = 90.0;
        soma[2][1] = 110.0;
        
        Matriz auxprod = new Matriz (2,3);
        double[][] prod = auxprod.getMatriz(); 
        prod[0][0] = 20.0;
        prod[0][1] = 40.0;
        prod[0][2] = 60.0;
        prod[1][0] = 80.0;
        prod[1][1] = 100.0;
        prod[1][2] = 120.0;

        Matriz transp = orig.getTransposta();
        Matriz s = orig.soma(auxsoma);
        
        Matriz mult = orig.prod(auxprod);
        
        
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz transposta: " + transp);
        
        System.out.println(auxsoma);
        System.out.println(s);
        System.out.println(auxprod);
        System.out.println(mult);
    }
}
